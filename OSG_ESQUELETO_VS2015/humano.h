#pragma once

#include "constantes.h"
#include "punto.h"
#include <math.h>

//constantes para el humano
#define CTE_GIRO 0.05
#define CTE_AVANCE 0.2

class humano : public punto{
	
	private:
		double direccion; //�ngulo en radianes
		bool avance;
		bool retroceso;
		bool giro_izq;
		bool giro_der;
		bool salto;
		double velz;

	public:
		humano(double a, double b, double c, double teta);
		//void girar(double teta);
		//void avanzar(double x);
		void setAvance(bool a) { avance = a; };
		void setRetroceso(bool a) {retroceso = a; };
		void setGiroIzq(bool a) { giro_izq = a; };
		void setGiroDer(bool a) { giro_der = a; };
		void setSalto(bool a);
		void actua(double t);
		void setDireccion(double teta);
		double getDireccion();
		void setVelZ(double vz) { velz = vz; };
		double getVelZ() { return velz; };
};