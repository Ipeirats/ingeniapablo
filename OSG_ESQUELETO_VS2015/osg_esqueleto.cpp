﻿#pragma once

#define WIN32_LEAN_AND_MEAN    // stops windows.h including winsock.h
#include <winsock2.h>

#include "stdafx.h"
#include "FuncionesAyuda.h"

#include <string>
#include <math.h>
//#include <iostream>
#include <fstream>
#include <vector>
#include <array>
#include <iterator>

#include <osg/AutoTransform>
#include <osg/Billboard>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Group>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/LineWidth>
#include <osg/Material>
#include <osg/MatrixTransform>
#include <osg/Node>
#include <osg/Notify>
#include <osg/PositionAttitudeTransform>
#include <osg/Projection>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/TexEnv>
#include <osg/TexGen>
#include <osg/TexMat>
#include <osg/Texture2D>
#include <osgText/Text>

#include <osgDB/ReadFile>
#include <osgDB/Registry>
#include <osgDB/WriteFile>

#include <osgGA/TrackballManipulator>

#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowVolume>
#include <osgShadow/ShadowTexture>
#include <osgShadow/ShadowMap>
#include <osgShadow/SoftShadowMap>
#include <osgShadow/ParallelSplitShadowMap>
#include <osgShadow/LightSpacePerspectiveShadowMap>
#include <osgShadow/StandardShadowMap>
#include <osgShadow/ViewDependentShadowMap>

#include <osgViewer/Viewer>

#include "constantes.h"
#include "punto.h"
#include "humano.h"

#include "rapidxml-1.13/rapidxml.hpp"

humano Yo(0, 0, 0, 0);
punto Camara(0, 0, 0);
int scroll_times = 0;
bool cam_detras = true;
bool cam_detras_en_cabeza = false;
void calcularPosCamara() {
	double angulo = Yo.getDireccion();
	double cam_dist = CAM_DIST - CAM_DIST_VAR*scroll_times;
	if(cam_dist<0) cam_detras=false;
	else cam_detras=true;
	if (cam_dist<0.5 && cam_dist>=0) cam_detras_en_cabeza = true;
	else cam_detras_en_cabeza = false;
	Camara.setX(Yo.getX() - cam_dist*cos(angulo));
	Camara.setY(Yo.getY() - cam_dist*sin(angulo));
	Camara.setZ(Yo.getZ() + cam_dist*sin(CAM_ANG*PI/180));
}


/*string getString() {
	string str = "string vacia";

	xml_document<> doc;    // character type defaults to char
	doc.parse<0>("prueba.xml");    // 0 means default parse flags

	cout << "Name of my first node is: " << doc.first_node()->name() << "\n";
	/*xml_node<> *node = doc.first_node("tre");
	cout << "Node foobar has value " << node->value() << "\n";
	for (xml_attribute<> *attr = node->first_attribute();
	attr; attr = attr->next_attribute())
	{
	cout << "Node foobar has attribute " << attr->name() << " ";
	cout << "with value " << attr->value() << "\n";
	}*/

	/*return str;
}*/

/*char* file_to_char_array(std::string file_name) {
	int num_characters = 0;
	std::ifstream myfile(file_name);

	if (myfile.is_open())
	{
		char c;
		//myfile.get(c);
		/*while (myfile.get(c))
		{
			num_characters++;
			//std::cout << c;
		}
		myfile.clear();
		myfile.seekg(0, myfile.beg); //para leer el archivo otra vez desde el principio
		char* array = new char[num_characters + 1];
		int i = 0;
		while (myfile.get(c))
		{
			array[i] = c;
			//std::cout << c;
			i++;
		}
		array[i] = '\0';
		myfile.close();
		//return array;
	}
	//else std::cout << "Unable to open file";

	return NULL;
}*/




std::string str_mostrar = "hola";

class SimulacionKeyboardEvent: public osgGA::GUIEventHandler
{
protected:
public:
	SimulacionKeyboardEvent::SimulacionKeyboardEvent()
	{		
	};
	struct control_teclado
	{
		double x;
		double y;
		
	} tecla_pulsada;
	
	virtual bool SimulacionKeyboardEvent::handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter&)
    {
	    int tecla = ea.getKey();
		int evento = ea.getEventType();
		int shift = ea.getModKeyMask();
		int button = ea.getButton();
		int scroll = ea.getScrollingMotion();

		//printf("evento: %d\n", evento);
		switch (evento)
		{
			//std::cout << evento << endl;
			case(osgGA::GUIEventAdapter::KEYDOWN):	//Evento pulsacion tecla
			{
				printf("tecla %c pulsada\n", tecla);
				switch (tecla)
				{
					case 'w':
						//Yo.avanzar(AVANCE);
						Yo.setAvance(true);
						Yo.setRetroceso(false);
						break;
					case 's':
						//Yo.avanzar(-AVANCE);
						Yo.setAvance(false);
						Yo.setRetroceso(true);
						break;
					case 'd':
						//Yo.girar(-GIRO);
						Yo.setGiroDer(true);
						Yo.setGiroIzq(false);
						break;
					case 'a':
						//Yo.girar(GIRO);
						Yo.setGiroDer(false);
						Yo.setGiroIzq(true);
						break;
					case 32: //barra espaciadora
						Yo.setSalto(true);
						break;
					default:
						break;
				}
				break;
			}
			case(osgGA::GUIEventAdapter::KEYUP):	// Se deja de pulsar una tecla
			{
				printf("tecla %c levantada\n", tecla);
				switch (tecla)
				{
				case 'w':
					//Yo.avanzar(AVANCE);
					Yo.setAvance(false);
					break;
				case 's':
					//Yo.avanzar(-AVANCE);
					Yo.setRetroceso(false);
					break;
				case 'd':
					//Yo.girar(-GIRO);
					Yo.setGiroDer(false);
					break;
				case 'a':
					//Yo.girar(GIRO);
					Yo.setGiroIzq(false);
					break;
				default:
					break;
					/*case 'c':
						tecla_pulsada.x += 1;
						break;
					case 'v':
						tecla_pulsada.x -= 1;
						break;
					default:
						break;*/
				}
				break;
			}
			case(osgGA::GUIEventAdapter::SCROLL):
			{
				printf("scroll motion: %d\n", scroll);
				switch (scroll)
				{
					case osgGA::GUIEventAdapter::SCROLL_UP: //=3
						if(cam_detras && !cam_detras_en_cabeza)
							scroll_times++;
						break;
					case osgGA::GUIEventAdapter::SCROLL_DOWN: //=4
						scroll_times--;
						break;
				}
				break;
			}
		}
		return 0;
	}
};

int main(int, char**)
{
	//string str1 = getString();

	osgViewer::Viewer viewer;
	viewer.setUpViewInWindow(50,50,800,600,0);
	osg::Group * grupo = new osg::Group;	

	calcularPosCamara();

	SimulacionKeyboardEvent* KeyboardEvent = new SimulacionKeyboardEvent();
	viewer.addEventHandler(KeyboardEvent);

	/*osg::Geode* malla = CreaMalla(512,1);											// Crea la malla del suelo
	AddTexture(malla, ".\\Texturas\\cesped.dds", 0);		// Añade la textura a la malla
    AddTexture(malla, ".\\Texturas\\Heightmap.png", 1);	// Añade la textura de altura
	grupo->addChild(malla);															//Añade malla al grupo
	//----------------------------- Luces ----------------------------------
	//Luz global
	osg::Light* myLight = new osg::Light;
	myLight->setLightNum(0);
	myLight->setPosition(osg::Vec4(0.0,400.0,1000.0,0.0f));				// Posicion foco
	myLight->setAmbient(osg::Vec4(1.0f,1.0f,1.0f,1.0f));				// Color luz ambiente
	myLight->setDiffuse(osg::Vec4(0.5f,0.5f,0.45f,1.0f));				// Color luz difusa
    myLight->setDirection(osg::Vec3(1.0f,0.0f,-1.0f));					// Direccion

	osg::LightSource* lightS = new osg::LightSource;
	lightS->setLight(myLight);
    lightS->setLocalStateSetModes(osg::StateAttribute::ON);

	osg::Group * grupo_luz = new osg::Group;
	grupo_luz->addChild(lightS);
	grupo->addChild(grupo_luz);

	//------------------------------ Sombras ------------------------------------	
	
	osgShadow::ShadowedScene* sc = new  osgShadow::ShadowedScene;
	osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm = new osgShadow::LightSpacePerspectiveShadowMapVB;

	sc->setShadowTechnique(sm);

	float minLightMargin = 10.f;
	float maxFarPlane = 1000;
	unsigned int texSize = 1024;
	unsigned int baseTexUnit = 0;
	unsigned int shadowTexUnit = 6;

	texSize = 4096;
	sm->setLight(myLight);
	sm->setMinLightMargin( minLightMargin );
	sm->setMaxFarPlane( maxFarPlane );
 	sm->setTextureSize( osg::Vec2s( texSize, texSize ) );		
	sm->setShadowTextureCoordIndex( shadowTexUnit );
	sm->setShadowTextureUnit( shadowTexUnit );
	sm->setBaseTextureCoordIndex( baseTexUnit );
	sm->setBaseTextureUnit( baseTexUnit );

	const int ReceivesShadowTraversalMask = 0x1;
	const int CastsShadowTraversalMask = 0x2;

	sc->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
    sc->setCastsShadowTraversalMask(CastsShadowTraversalMask);

	grupo->addChild(sc);*/
	
	viewer.setSceneData(grupo);
	
	//std::cout << "hola!!!";

	//--------------------------- Camara_texto ------------------------------- 
	osg::Camera* camera_texto = new osg::Camera;
	camera_texto->setProjectionMatrix(osg::Matrix::ortho2D(-400,400,-300,300));	
	camera_texto->setReferenceFrame(osg::Transform::ABSOLUTE_RF);			
	camera_texto->setViewMatrix(osg::Matrix::identity());
	camera_texto->setClearMask(GL_DEPTH_BUFFER_BIT);						
	camera_texto->setRenderOrder(osg::Camera::POST_RENDER);				
	grupo->addChild(camera_texto);

	//----------------------------- Texto ----------------------------
	osg::ref_ptr<osgText::Font> font = osgText::readRefFontFile("fonts/arial.ttf");
	osg::Geode* geode_texto_intro = new osg::Geode();
	osgText::Text* texto_intro = new osgText::Text;
	geode_texto_intro->addDrawable(texto_intro);
	camera_texto->addChild(geode_texto_intro);

	texto_intro->setFont(font);
	texto_intro->setPosition(osg::Vec3(0.0f,0.0f,0.0f));
	texto_intro->setFontResolution(40,40);
	texto_intro->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
	texto_intro->setCharacterSize(20);
	texto_intro->setAlignment(osgText::Text::LEFT_BOTTOM);
	texto_intro->setText("hola");

	//-----------Codigo editado---------//
	//std::cout << "hola";
	printf("bienvenido\n");

	std::string geometria1 = "Tree.obj";
	std::string geometria2 = "Palma 001.obj";
	std::string geometria3 = "FinalBaseMesh.obj";
	osg::Node* nodo = osgDB::readNodeFile(".\\Texturas\\Trees\\" + geometria1);//lee en la carpeta tree y le añade la geometria 
	osg::Node* nodo2 = osgDB::readNodeFile(".\\Texturas\\Trees\\"+ geometria2);
	osg::Node* nodoH = osgDB::readNodeFile(".\\Texturas\\Humanos\\"+ geometria3);
	//grupo->addChild(nodo); 
	
	osg::MatrixTransform* mt = new osg::MatrixTransform;
	mt->addChild(nodo);
	grupo->addChild(mt);
	osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos(10, 0, 0);
	mtras = mtras.translate(pos);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.1, 0.1, 0.1);
	mtotal = mesc*mrot*mtras;
	mt->setMatrix(mtotal);

	osg::MatrixTransform* mt2 = new osg::MatrixTransform;
	mt2->addChild(nodo);
	grupo->addChild(mt2);
	//osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos2(-10, 0, 0);
	mtras = mtras.translate(pos2);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.12, 0.12, 0.12);
	mtotal = mesc*mrot*mtras;
	mt2->setMatrix(mtotal);

	osg::MatrixTransform* mt3 = new osg::MatrixTransform;
	mt3->addChild(nodo2);
	grupo->addChild(mt3);
	//osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d pos3(0, -5, 0);
	mtras = mtras.translate(pos3);
	mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(0.1, 0.1, 0.1);
	mtotal = mesc*mrot*mtras;
	mt3->setMatrix(mtotal);

	/*****xml inicio****/

	rapidxml::xml_document<> doc;
	// Read the xml file into a vector
	std::ifstream theFile(".\\trees.xml");
	std::vector<char> buffer((std::istreambuf_iterator<char>(theFile)), std::istreambuf_iterator<char>());
	buffer.push_back('\0');
	// Parse the buffer using the xml file parsing library into doc 
	doc.parse<0>(&buffer[0]);
	
	rapidxml::xml_node<> *nodo0;
	nodo0 = doc.first_node();
	

	int i = 0; //numero de nodos1
	for (rapidxml::xml_node<>* nodo1 = nodo0->first_node(); nodo1; nodo1 = nodo1->next_sibling()) {
		i++;
	}
	int num_nodos = i;

	i = 0;
	for (rapidxml::xml_node<>* nodo1 = nodo0->first_node(); nodo1; nodo1 = nodo1->next_sibling()) {
		
		printf("\tnodo %d (", i);
		int j = 0;
		char *c = nodo1->name();
		while (c[j] != '\0') { printf("%c", c[j]); j++; }
		printf(") tiene:\n");
		i++;

		osg::MatrixTransform* mt = new osg::MatrixTransform;

		std::string str = nodo1->first_node("fichero")->value();
		std::string strX= nodo1->first_node("x")->value();
		std::string strY= nodo1->first_node("y")->value();
		std::string strZ= nodo1->first_node("z")->value();
		osg::Node* nodoG = osgDB::readNodeFile(".\\Texturas\\Trees\\" + str);

	//NOS QUEDAMOS AQUÍ
		grupo->addChild(mt);
		osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
		osg::Vec3d pos(10, 0, 0);
		mtras = mtras.translate(pos);
		mrotz = mrotz.rotate(0, osg::Vec3d(0, 0, 1));
		mesc = mtras.scale(0.1, 0.1, 0.1);
		mtotal = mesc*mrot*mtras;
		mt->setMatrix(mtotal);
		for (rapidxml::xml_node<> *nodo2 = nodo1->first_node(); nodo2; nodo2 = nodo2->next_sibling())
		{
			//std::cout << "\t\t" << nodo2->name() << "=" << nodo2->value() << "\n";
			printf("\t\t");
			int k = 0;
			char *cc = nodo2->name();
			while (cc[k] != '\0') { printf("%c", cc[k]); k++; }
			printf("=");
			cc = nodo2->value();
			k = 0;
			while (cc[k] != '\0') { printf("%c", cc[k]); k++; }
			printf("\n");

			if (nodo2->name())
			
		}
	}
	printf("numero de nodos: %d", num_nodos);

	/*****xml fin******/


	osg::MatrixTransform* mtH = new osg::MatrixTransform;
	mtH->addChild(nodoH);
	grupo->addChild(mtH);
	//osg::Matrixd mtras, mrot, mrotz, mrotx, mroty, mesc, mtotal, mtras2;
	osg::Vec3d posH(Yo.getX(), Yo.getY(), Yo.getZ());
	mtras = mtras.translate(posH);
	mrotz = mrotz.rotate(Yo.getDireccion()+PI/2, osg::Vec3d(0, 0, 1));
	mesc = mtras.scale(.1, .1, .1);
	mtotal = mesc*mrot*mtras;
	mtH->setMatrix(mtotal);

	double t1=0,t0=0;
	viewer.realize();
	while (!viewer.done()) {

		t0 = t1;
		t1 = ::GetCurrentTime();
		Yo.actua((t1-t0)/1000);
		posH.set(Yo.getX(), Yo.getY(), Yo.getZ());
		mtras = mtras.translate(posH);
		mrotz = mrotz.rotate(Yo.getDireccion()+PI/2, osg::Vec3d(0, 0, 1));
		mesc = mtras.scale(.1, .1, .1);
		mtotal = mesc*mrotz*mtras;
		mtH->setMatrix(mtotal);

		calcularPosCamara();
		if(cam_detras && cam_detras_en_cabeza){//hay que poner el vector con signo contrario
			//coger el vector Yo-Camara, normalizarlo y ponerlo delante de Yo
			double x1 = Yo.getX(); //1 para Yo y 2 para Camara
			double y1 = Yo.getY();
			double z1 = Yo.getZ();
			double x2 = Camara.getX();
			double y2 = Camara.getY();
			double z2 = Camara.getZ();
			double dx = x2 - x1;
			double dy = y2 - y1;
			double dz = z2 - z1;
			double d = sqrt(dx*dx + dy*dy + dz*dz); 
			viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Yo.getX() - dx / d*0.2, Yo.getY() - dy / d*0.2, Yo.getZ() - dz / d*0.2 + 2), //desde dónde miramos
				osg::Vec3d(osg::Vec3d(Yo.getX() - dx / d, Yo.getY() - dy / d, Yo.getZ() - dz / d + 2)),										 //hacia dónde miramos
				osg::Vec3d(0, 0, 1));																									 //vector vertical
			}
		else if(cam_detras)
			viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Camara.getX(), Camara.getY(), Camara.getZ()+2), //desde dónde miramos
													osg::Vec3d(Yo.getX(), Yo.getY(), Yo.getZ()+2),			    //hacia dónde miramos
													osg::Vec3d(0, 0, 1));									    //vector vertical
		else {
			//coger el vector Yo-Camara, normalizarlo y ponerlo delante de Yo
			double x1 = Yo.getX(); //1 para Yo y 2 para Camara
			double y1 = Yo.getY();
			double z1 = Yo.getZ();
			double x2 = Camara.getX();
			double y2 = Camara.getY();
			double z2 = Camara.getZ();
			double dx = x2 - x1;
			double dy = y2 - y1;
			double dz = z2 - z1;
			double d = sqrt(dx*dx + dy*dy + dz*dz);
				viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(Yo.getX()+dx/d*0.2, Yo.getY()+dy/d*0.2, Yo.getZ()+dz/d*0.2 +2), //desde dónde miramos
					osg::Vec3d(osg::Vec3d(Yo.getX() + dx/d, Yo.getY() + dy/d, Yo.getZ() + dz/d + 2)),								 //hacia dónde miramos
					osg::Vec3d(0, 0, 1));																							 //vector vertical
		}
		Sleep(33);
		viewer.frame();





	}


	return 0;

	}

