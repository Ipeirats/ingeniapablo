#pragma once
class Movil
{
	double x, y;

	public:
		Movil();
		Movil(double _x, double _y);
		~Movil();
		void setX(double a);
		void setY(double b);
		double getX();
		double getY();
		virtual void simular(double inc_t); //virtual sirve para dar preferencia a los m�todos de las clases derivadas
};

