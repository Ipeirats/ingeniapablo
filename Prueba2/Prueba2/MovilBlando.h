#pragma once

#include "Movil.h"

class MovilBlando : public Movil
{
	public:
		MovilBlando();
		~MovilBlando();
		void simular(double inc_t);
};

